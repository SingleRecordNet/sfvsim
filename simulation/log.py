import logging

logging.basicConfig(level=logging.INFO)
transition_logger = logging.getLogger('transition')
position_logger = logging.getLogger('state')
hit_logger = logging.getLogger('hit')

transition_logger.setLevel(logging.INFO)
hit_logger.setLevel(logging.INFO)
position_logger.setLevel(logging.INFO)
